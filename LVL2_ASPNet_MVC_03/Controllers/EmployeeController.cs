﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();
        public ActionResult index()
        {
            return View(db.Employees.ToList());
        }


        //========================
        public ActionResult Details(int id)
        {
            return View(db.Employees.Where(x=> x.id == id).FirstOrDefault());
        }
        public ActionResult Edit(int id)
        {
            return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
        }
        public ActionResult Delete(int id)
        {
            return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
        }
        //========================
        [HttpPost]
        public ActionResult Edit(int id, Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }
        [HttpPost]
        public ActionResult Delete(int id, Employee employee)
        {
            if (ModelState.IsValid)
            {
                employee = db.Employees.Where(x => x.id == id).FirstOrDefault();
                db.Employees.Remove(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
           // if (ModelState.IsValid)
           // {
                using (var Transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var remove1employee = db.Employees.Where(x => x.id == 1).FirstOrDefault();
                        db.Employees.Remove(remove1employee);
                        db.Employees.Add(employee); 
                        db.SaveChanges();
                        Transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                    Transaction.Rollback();
                    }
                }  
            //}
            return View(employee);
        }

        
    }
}